import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  useEnglish = false;
  appForm: any;
  isLoading = false;

  constructor(private translate: TranslateService,
              private fb: FormBuilder) {
    translate.setDefaultLang('vi');
    this.appForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      firstName: ['', Validators.required],
      lastName: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      age: ['', [Validators.min(0), Validators.max(200)]]
    });
  }

  switchLanguage() {
    if(this.useEnglish === true) {
      this.translate.use('vi');
      this.useEnglish = false;
    } else {
      this.translate.use('en');
      this.useEnglish = true;
    }
  }

  demoSpinner() {
    if (this.isLoading === false) {
      this.isLoading = true;
      setTimeout(() => {
        this.isLoading = false
      }, 2000);
    }
  }

}
