import { Directive, ElementRef, Input, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[lineClamp]'
})
export class LineClampDirective implements AfterViewInit{
  @Input() line: number;

  constructor(private el: ElementRef) {
   }

   ngAfterViewInit() {
    this.el.nativeElement.style.display = '-webkit-box';
    this.el.nativeElement.style.overflow = 'hidden';
    this.el.nativeElement.style.textOverflow = 'ellipsis';
    this.el.nativeElement.style.webkitBoxOrient = 'vertical';
    this.el.nativeElement.style.webkitLineClamp = this.line;
    this.el.nativeElement.style.wordBreak = 'break-word';
   }

}
